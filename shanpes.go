package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	shapePoints, shapeEdges, _ := makeIcosasphere(1)
	scaleToUnitMinEdgeLength(shapePoints)
	points, _, faces := getBallAndStick(shapePoints, shapeEdges, 0.1, 0.1, 2, 24)
	outFile, err := os.OpenFile("./out/icosasphere-q1-sticks.obj", os.O_WRONLY|os.O_CREATE|os.O_EXCL, os.ModePerm)

	// points := makeDodecahedron()
	// scaleToUnitMinEdgeLength(points)
	// edges := getEdges(points, 1.1)
	// faces := getConvexFaces(points, edges, 5)
	// outFile, err := os.OpenFile("./out/dodecahedron.obj", os.O_WRONLY|os.O_CREATE|os.O_EXCL, os.ModePerm)

	if err != nil {
		log.Fatalln("Error opening output file:", err)
	}
	err = writeObj(outFile, points, faces)
	if err != nil {
		log.Fatalln("Error writing output file:", err)
	}
	err = outFile.Close()
	if err != nil {
		log.Fatalln("Error closing output file:", err)
	}
}

func writeObj(writer io.Writer, points []Vec3, faces []Face) error {
	for _, point := range points {
		_, err := fmt.Fprintf(writer, "v %.6f %.6f %.6f\n", point[0], point[1], point[2])
		if err != nil {
			return fmt.Errorf("writing vertex: %v", err)
		}
	}
	for _, face := range faces {
		_, err := fmt.Fprintln(writer, face.objString())
		if err != nil {
			return fmt.Errorf("writing face: %v", err)
		}
	}
	return nil
}

func scaleToUnitMinEdgeLength(points []Vec3) {
	minEdgeLength := math.Inf(1)
	for i := 0; i < len(points)-1; i++ {
		for j := i + 1; j < len(points); j++ {
			edgeLength := points[i].distance(points[j])
			if edgeLength < minEdgeLength {
				minEdgeLength = edgeLength
			}
		}
	}
	for i := range points {
		points[i] = points[i].scalarQuotient(minEdgeLength)
	}
}

func getEdges(points []Vec3, threshold float64) []Edge {
	edges := make([]Edge, 0)
	for i := 0; i < len(points)-1; i++ {
		for j := i + 1; j < len(points); j++ {
			if points[i].distance(points[j]) < threshold {
				edges = append(edges, Edge{i, j})
			}
		}
	}
	return edges
}

func getConvexFaces(points []Vec3, edges []Edge, numFaceSides int) []Face {
	faces := make([]Face, 0)
	edgeCounts := make(map[Edge]int, len(edges))
	neighbors := make(map[int]map[int]bool, len(points))
	for i := range points {
		neighbors[i] = make(map[int]bool, (len(edges)*2)/len(points))
	}
	for _, edge := range edges {
		edgeCounts[edge.canonical()] = 2
		neighbors[edge[0]][edge[1]] = true
		neighbors[edge[1]][edge[0]] = true
	}
	currentPathMap := make(map[int]bool)
	shapeCenter := averageVec3(points)
	seenFaceStrings := make(map[string]bool)
	madeProgress := true
	for len(edgeCounts) > 0 {
		if !madeProgress {
			panic(errors.New("the getConvexFaces algorithm would have gotten stuck in an endless loop with the given points and faces"))
		}
		madeProgress = false
	edgeLoop:
		for edge := range edgeCounts {
			for _, pointIndex := range edge {
				face := Face(getGraphCycle(neighbors, pointIndex, pointIndex, numFaceSides, currentPathMap))
				for n := range currentPathMap {
					delete(currentPathMap, n)
				}
				center := face.center(points)
				direction := points[face[0]].crossProduct(points[face[1]]).normal()
				if center.plus(direction).distance(shapeCenter) > center.minus(direction).distance(shapeCenter) {
					face = face.reverse()
				}
				face = face.canonical()
				faceString := fmt.Sprint(face)
				if face == nil || seenFaceStrings[faceString] {
					continue
				}
				madeProgress = true
				seenFaceStrings[faceString] = true
				faces = append(faces, face)
				for i := range face {
					faceEdge := Edge{face[i], face[(i+1)%len(face)]}.canonical()
					edgeCounts[faceEdge]--
					if edgeCounts[faceEdge] == 0 {
						delete(edgeCounts, faceEdge)
						delete(neighbors[faceEdge[0]], faceEdge[1])
						delete(neighbors[faceEdge[1]], faceEdge[0])
					}
				}
				break edgeLoop
			}
		}
	}
	return faces
}

func getGraphCycle(graph map[int]map[int]bool, src, dst, numEdges int, currentPath map[int]bool) []int {
	if numEdges == 0 {
		if src == dst {
			return []int{}
		}
		return nil
	}
	for neighbor := range graph[src] {
		if currentPath[neighbor] {
			continue
		}
		currentPath[neighbor] = true
		subpath := getGraphCycle(graph, neighbor, dst, numEdges-1, currentPath)
		delete(currentPath, neighbor)
		if subpath != nil {
			return append([]int{src}, subpath...)
		}
	}
	return nil
}

func getBallAndStick(points []Vec3, edges []Edge, ballRadius, stickRadius float64, ballQuality, numStickSides int) ([]Vec3, []Edge, []Face) {
	outPoints := make([]Vec3, 0, 12*len(points)+numStickSides*2*len(edges))
	outEdges := make([]Edge, 0, 30*len(points)+numStickSides*3*len(edges))
	outFaces := make([]Face, 0, 20*len(points)+numStickSides*len(edges))
	for _, point := range points {
		ballPoints, ballEdges, ballFaces := makeIcosasphere(ballQuality)
		for i := range ballPoints {
			ballPoints[i] = point.plus(ballPoints[i].scalarProduct(ballRadius))
		}
		appendIsolatedParts(&outPoints, &outEdges, &outFaces, ballPoints, ballEdges, ballFaces)
	}
	for _, edge := range edges {
		stickPoints, stickEdges, stickFaces := makeCylinder(points[edge[0]], points[edge[1]], stickRadius, numStickSides)
		appendIsolatedParts(&outPoints, &outEdges, &outFaces, stickPoints, stickEdges, stickFaces)
	}
	return outPoints, outEdges, outFaces
}

func appendIsolatedParts(dstPoints *[]Vec3, dstEdges *[]Edge, dstFaces *[]Face, srcPoints []Vec3, srcEdges []Edge, srcFaces []Face) {
	numDstPoints := len(*dstPoints)
	*dstPoints = append(*dstPoints, srcPoints...)
	for _, srcEdge := range srcEdges {
		*dstEdges = append(*dstEdges, Edge{srcEdge[0] + numDstPoints, srcEdge[1] + numDstPoints})
	}
	for _, srcFace := range srcFaces {
		dstFace := make(Face, len(srcFace))
		for j := 0; j < len(dstFace); j++ {
			dstFace[j] = srcFace[j] + numDstPoints
		}
		*dstFaces = append(*dstFaces, dstFace)
	}
}

const tau = math.Pi * 2

// Returns an open cylinder; point1 and point2 are the centers of the ends.
// The line defined by point1 and point2 cannot pass through the origin.
func makeCylinder(point1, point2 Vec3, radius float64, numSides int) ([]Vec3, []Edge, []Face) {
	points := make([]Vec3, numSides*2)
	edges := make([]Edge, numSides*3)
	faces := make([]Face, numSides)
	for i := 0; i < numSides; i++ {
		basis := Vec3{}.lineProjection(point1, point2).normal()
		angle := (tau * float64(i)) / float64(numSides)
		offset := basis.scalarProduct(math.Cos(angle)).plus(
			basis.crossProduct(point2.minus(point1)).normal().scalarProduct(math.Sin(angle)),
		).scalarProduct(radius)
		points[i*2] = point1.plus(offset)
		points[i*2+1] = point2.plus(offset)
		edges[i*3] = Edge{i * 2, ((i + 1) * 2) % (numSides * 2)}.canonical()
		edges[i*3+1] = Edge{i * 2, i*2 + 1}.canonical()
		edges[i*3+2] = Edge{i*2 + 1, ((i+1)*2 + 1) % (numSides * 2)}.canonical()
		faces[i] = Face{i * 2, ((i + 1) * 2) % (numSides * 2), ((i+1)*2 + 1) % (numSides * 2), i*2 + 1}.canonical()
	}
	return points, edges, faces
}

// Returns a repeatedly subdivided and sphere-projected icosahedron
func makeIcosasphere(quality int) ([]Vec3, []Edge, []Face) {
	points := makeIcosahedron()
	for i := range points {
		points[i] = points[i].normal()
	}
	edges := getEdges(points, 1.1)
	faces := getConvexFaces(points, edges, 3)
	newPoints := make([]Vec3, 0, len(points))
	newEdges := make([]Edge, 0, len(edges))
	newFaces := make([]Face, 0, len(faces))
	edgeCenters := make(map[Edge]int)
	for quality > 0 {
		newPoints = append(newPoints, points...)
		for _, edge := range edges {
			center := points[edge[0]].plus(points[edge[1]]).scalarQuotient(2).normal()
			edgeCenters[edge] = len(newPoints)
			newEdges = append(newEdges, Edge{edge[0], len(newPoints)})
			newEdges = append(newEdges, Edge{edge[1], len(newPoints)})
			newPoints = append(newPoints, center)
		}
		for _, face := range faces {
			for i := range face {
				edgeCenterForward := edgeCenters[Edge{face[i], face[(i+1)%3]}.canonical()]
				edgeCenterBackward := edgeCenters[Edge{face[i], face[(i+2)%3]}.canonical()]
				newEdges = append(newEdges, Edge{edgeCenterForward, edgeCenterBackward}.canonical())
				newFaces = append(newFaces, Face{face[i], edgeCenterForward, edgeCenterBackward}.canonical())
			}
			newFaces = append(newFaces, Face{
				edgeCenters[Edge{face[0], face[1]}.canonical()],
				edgeCenters[Edge{face[1], face[2]}.canonical()],
				edgeCenters[Edge{face[2], face[0]}.canonical()],
			}.canonical())
		}
		for edge := range edgeCenters {
			delete(edgeCenters, edge)
		}
		points, newPoints = newPoints, points[:0]
		edges, newEdges = newEdges, edges[:0]
		faces, newFaces = newFaces, faces[:0]
		quality--
	}
	return points, edges, faces
}

var minusPlusOne = [2]float64{-1, +1}

func makeTetrahedron() []Vec3 {
	points := make([]Vec3, 4)
	for i := 0; i < 3; i++ {
		points[i][i] = 1
		points[3][i] = 1
	}
	return points
}

func makeOctahedron() []Vec3 {
	points := make([]Vec3, 6)
	p := 0
	for i := 0; i < 3; i++ {
		for j := 0; j < 2; j++ {
			points[p][i] = minusPlusOne[j]
			p++
		}
	}
	return points
}

func makeCube() []Vec3 {
	points := make([]Vec3, 8)
	p := 0
	for i := 0; i < 8; i++ {
		for j := uint(0); j < 3; j++ {
			points[p][j] = minusPlusOne[(i>>j)&1]
		}
		p++
	}
	return points
}

func makeIcosahedron() []Vec3 {
	points := make([]Vec3, 12)
	p := 0
	for i := 0; i < 3; i++ {
		for j := uint(0); j < 4; j++ {
			points[p][(1+i)%3] = minusPlusOne[j&1] * math.Phi
			points[p][(2+i)%3] = minusPlusOne[(j>>1)&1]
			p++
		}
	}
	return points
}

func makeDodecahedron() []Vec3 {
	points := make([]Vec3, 20)
	p := 0
	for i := 0; i < 8; i++ {
		for j := uint(0); j < 3; j++ {
			points[p][j] = minusPlusOne[(i>>j)&1]
		}
		p++
	}
	for i := 0; i < 3; i++ {
		for j := uint(0); j < 4; j++ {
			points[p][(1+i)%3] = minusPlusOne[j&1] * math.Phi
			points[p][(2+i)%3] = minusPlusOne[(j>>1)&1] / math.Phi
			p++
		}
	}
	return points
}

// Vec3 is {x, y, z} of a 3D vector
type Vec3 [3]float64

func (p1 Vec3) magnitude() float64 {
	return math.Sqrt(p1[0]*p1[0] + p1[1]*p1[1] + p1[2]*p1[2])
}

func (p1 Vec3) normal() Vec3 {
	return p1.scalarQuotient(p1.magnitude())
}

func (p1 Vec3) distance(p2 Vec3) float64 {
	dx, dy, dz := p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2]
	return math.Sqrt(dx*dx + dy*dy + dz*dz)
}

func (p1 Vec3) plus(p2 Vec3) Vec3 {
	return Vec3{p1[0] + p2[0], p1[1] + p2[1], p1[2] + p2[2]}
}

func (p1 Vec3) minus(p2 Vec3) Vec3 {
	return Vec3{p1[0] - p2[0], p1[1] - p2[1], p1[2] - p2[2]}
}

func (p1 Vec3) dotProduct(p2 Vec3) float64 {
	return p1[0]*p2[0] + p1[1]*p2[1] + p1[2]*p2[2]
}

func (p1 Vec3) crossProduct(p2 Vec3) Vec3 {
	return Vec3{p1[1]*p2[2] - p1[2]*p2[1], p1[2]*p2[0] - p1[0]*p2[2], p1[0]*p2[1] - p1[1]*p2[0]}
}

func (p1 Vec3) scalarProduct(n float64) Vec3 {
	return Vec3{p1[0] * n, p1[1] * n, p1[2] * n}
}

func (p1 Vec3) scalarQuotient(n float64) Vec3 {
	return Vec3{p1[0] / n, p1[1] / n, p1[2] / n}
}

func (p1 Vec3) lineProjection(a, b Vec3) Vec3 {
	ab := b.minus(a)
	return a.plus(ab.scalarProduct(p1.minus(a).dotProduct(ab)).scalarQuotient(ab.dotProduct(ab)))
}

func averageVec3(points []Vec3) Vec3 {
	sum := Vec3{}
	for _, point := range points {
		sum = sum.plus(point)
	}
	return sum.scalarQuotient(float64(len(points)))
}

// Edge is indices of 2 points in a known list of points
type Edge [2]int

func (edge Edge) canonical() Edge {
	if edge[0] < edge[1] {
		return edge
	}
	return Edge{edge[1], edge[0]}
}

// Face is indices of 3 or more points in a known list of points
type Face []int

func (face Face) canonical() Face {
	if len(face) <= 1 {
		return face
	}
	minIndex, min := 0, face[0]
	for i, n := range face {
		if n < min {
			minIndex, min = i, n
		}
	}
	canonFace := make(Face, len(face))
	numCopied := copy(canonFace, face[minIndex:])
	copy(canonFace[numCopied:], face)
	return canonFace
}

func (face Face) reverse() Face {
	reverseFace := make(Face, len(face))
	for i := range reverseFace {
		reverseFace[i] = face[len(face)-1-i]
	}
	return reverseFace
}

func (face Face) center(vertices []Vec3) Vec3 {
	sum := Vec3{}
	for _, index := range face {
		sum = sum.plus(vertices[index])
	}
	return sum.scalarQuotient(float64(len(face)))
}

func (face Face) objString() string {
	builder := &strings.Builder{}
	builder.WriteString("f")
	for _, index := range face {
		builder.WriteString(" ")
		builder.WriteString(strconv.Itoa(index + 1))
	}
	return builder.String()
}
